<?php
namespace Genie\Components;


/**
* Class Component
*/
abstract class AbstractComponent {
  /**
   * @var Component
   */
  protected $component;

  /**
   * @var Config
   */
  protected $config;

  public function register($component){
    $this->component = $component;
  }

  public function initialize(array $config){
    $this->config = $config;
  }

}