<?php

namespace Genie\Components\Social;

use Genie\Components\AbstractComponent;
use League\OAuth2\Client\Provider\AccessToken as AccessToken;

abstract class FourSquareComponent extends SocialComponent implements SocialInterface {
  protected $provider;
  protected $config;

  public function __construct(array $config){
    $this->config = $config;
    $this->provider = new FourSquare($config);
  }

  public function getToken($code){
    try {
      
      $token = $this->provider->getAccessToken('authorization_code', array('code' => $code));
      return $token->getToken();
      
    } catch (Exception $e){
      Log::warning('FourSquare API: unable to get access token');
    }
  }

  public function setSession(array $api){
    try {
      Session::put('foursquare.oauth.token', $api['token']);
      Session::put('foursquare.users.url', $api['users']);
      Session::put('foursquare.venues.url', $api['venues']);
      Session::put('foursquare.location', Config::get('social.foursquare.location'));
    } catch (Exception $e){
      Log::warning('FourSquare API: unable to get user details');
    }
  }

  public function getLocation($locationId, $token){
    $param = http_build_query(array('v'           => date('Ymd'),
                                    'oauth_token' => $token->getToken()));

    $api['users'] = Config::get('social.foursquare.apiUri') . 
                    Config::get('social.foursquare.resource.users') . '?' . $param;
    $api['venues'] = Config::get('social.foursquare.apiUri') . 
                     Config::get('social.foursquare.resource.venues') . 
                     Config::get('social.foursquare.location') . '?' . $param;
    
    $this->client = new Client();
    $response = $this->client->get($api['venues']);
    $body = $response->getBody();
    $data = json_decode((string)$body);
    return $data;
  }
}