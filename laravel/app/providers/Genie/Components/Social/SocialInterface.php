<?php
namespace Genie\Components\Social;

use League\OAuth2\Client\Provider\AccessToken as AccessToken;

interface SocialInterface {

  public function getAccessUri(AccessToken $token);

  public function getUserDetailsUri();
  
}