<?php
namespace Genie\Components\Data;

class DataComponent {
  
  protected $connection;

  protected function createDatabase($database){
    if (App::environment() == 'testing'){
      $this->connection->statement("CREATE DATABASE IF NOT EXISTS `".$database."` ");
    }
  }

  protected function getDatabaseNames(){
    $databases = $this->connection->select('SHOW DATABASES');
    $database_data = array();
    foreach ($databases as $database){
      $database_data[] = $database->Database;
      unset($database);
    }
    return $database_data;
  }

  protected function getTableNames($original){
    $databases = $this->connection->select('SHOW DATABASES');
    $present = 0;
    $table_data = array();
    foreach ($databases as &$database){
      if ($database->Database == $original){
        $tables = $this->connection->select('SHOW TABLES');
        foreach ($tables as $table){
          $table = $table->Tables_in_db_main;
          $table_data[] = $table;
        }
      }
      unset($database);
    }
    return $table_data;
  }

  protected function cloneSchema($original, $copy){
    $table_data = array();
    if (App::environment() == 'testing'){
      $databases = $this->connection->select('SHOW DATABASES');
      foreach($databases as $database){
        if ($database->Database == $copy){
          foreach($this->getTableNames($original) as $table){
            $this->connection
                 ->statement("CREATE TABLE IF NOT EXISTS " . $copy . 
                             "." . $table . " LIKE ". $original . "." . $table . " ;"
                            ); 
          }
        }
      }
    }
  }
  
  protected function importData($original, $copy, $limit=100){
    if (App::environment() == 'testing'){
      $databases = $this->connection->select('SHOW DATABASES');
      foreach($databases as $database){
        foreach ($this->getTableNames($original) as $table){
          $this->connection->statement("REPLACE INTO $copy.$table SELECT * FROM $original.$table LIMIT $limit");
        }
      }
    }
  }
}