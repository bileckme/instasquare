<?php
namespace Genie\Components\Filesystem;

use Illuminate\Support\Facades\Config as Config;

use League\OAuth2\Client\Provider\GenericProvider;
use League\Flysystem\Dropbox\DropboxAdapter;
use League\Flysystem\Filesystem;
use Dropbox\Client;
use Dropbox;

/**
 * Class DropboxComponent
 */
class DropboxComponent extends AbstractComponent implements FilesystemInterface {
/**
   * @var Dropbox\Client
   * @access protected
   */
  protected $adapter;

  /**
   * @var Dropbox\Client
   * @access public
   */
  public $client;

  /**
   * @var Absolem\Component|string
   * @access protected
   */
  protected $component;

  /**
   * @var League\Flysystem\Filesystem
   * @access protected
   */
  protected $filesystem;

  /**
   * Registers the Dropbox component
   * @param string $component The component name
   * @return void
   */
  public function register($component='Dropbox'){
    $this->component = $component;
  }

  /**
   * TODO: Laravel OAuth2 authentication method implementation
   * @param array $config The Dropbox application configuration
   * @return void 
   */
  public function initialize(array $config, $prefix=null){

    $this->client = new Client($config);

    if (isset($prefix)){
      $this->adapter = new DropboxAdapter($this->client, $prefix);
    } else {
      $this->adapter = new DropboxAdapter($this->client);
    }
  
    $this->filesystem = new Filesystem($this->adapter);
  }

  /**
   * TODO: Laravel OAuth2 authentication method implementation
   * @param array $config The Dropbox application configuration
   * @return GenericProvider 
   */
  public function authorize(array $config){
    $provider = new GenericProvider($config);
    return $provider;
  }

  /**
   * Gets Dropbox API configuration
   * @param void
   * @return Dropbox\WebAuth The Dropbox web authorization object instance
   */
  private function getConfig(){
    try {
      $appInfo = Dropbox\AppInfo::loadFromJsonFile();
    }
    catch (Dropbox\AppInfoLoadException $ex) {
      Log::warning("Unable to load \"$appInfoFile\": " . $ex->getMessage());
    }

    $clientIdentifier = Config::get('social.dropbox.id');
    $userLocale = null;

    return array($appInfo, $clientIdentifier, $userLocale);
  }

  /**
   * Gets Dropbox web authorization without redirect
   * @param void
   * @return Dropbox\WebAuth The Dropbox web authorization object instance
   */
  private function webAuth(){
    $redirectUri = $this->getAuthorizeUrl();
    $csrfTokenStore = new Dropbox\ArrayEntryStore($_SESSION, 'dropbox-auth-csrf-token');
    list($appInfo, $clientIdentifier, $userLocale) = $this->getConfig();
    return new Dropbox\WebAuth($appInfo, $clientIdentifier, $redirectUri, $csrfTokenStore, $userLocale);
  }

  /**
   * Gets Dropbox web authorization without redirect
   * @param void
   * @return Dropbox\WebAuthNoRedirect The Dropbox no-redirect web authorization object instance
   */
  private function webAuthNoRedirect(){
    $appInfo = Dropbox\AppInfo::loadFromJsonFile(Config::get('social.dropbox.key'));
    return new Dropbox\WebAuthNoRedirect($appInfo, Config::get('social.dropbox.id'));
  }

  /**
   * Gets Dropbox authorization URL
   * @param void
   * @return string The Dropbox authorization url
   */
  public function getAuthorizeUrl(){
    return $this->webAuthNoRedirect()->start();
  }

  /**
   * Gets Dropbox access token
   * @param string $code The Dropbox authorization code
   * @return Dropbox\Client The Dropbox client
   */
  public function getAccessToken($code){
    return $this->webAuth()->finish($code);
  }

  /**
   * Gets Dropbox Client object
   * @param string $accessToken The Dropbox access token
   * @return Dropbox\Client The Dropbox client
   */
  public function getClient($accessToken){
    $this->client = new Dropbox\Client($accessToken, Config::get('social.dropbox.id'));
    return $this->client;
  }

  /**
   * Gets Dropbox account owner information
   * @param void
   * @return mixed The Dropbox account information
   */
  public function account(){
    if (is_object($this->client) && $this->client instanceof  Dropbox\Client) {
      return $this->client->getAccountInfo();
    }
    return false;
  }

  /**
   * Gets Dropbox folder information
   * @param string $path The folder path
   * @return mixed The Dropbox folder information
   */
  public function folder($path="/"){
    if (is_object($this->client) && $this->client instanceof  Dropbox\Client) {
      return $this->client->getMetadataWithChildren($path);
    }
    return false;
  }

  /**
   * Gets Dropbox file information
   * @param string $filename The file name
   * @return mixed The Dropbox file information
   */
  public function file($filename){
    if (is_object($this->client) && $this->client instanceof  Dropbox\Client) {
      $f = fopen($filename, "w+b");
      $fileMetadata = $this->client->getFile("/$filename", $f);
      fclose($f);
      return ($fileMetadata);
    }
    return false;
  }

  /**
   * Uploads file to Dropbox
   * @param string $file The file to upload
   * @return mixed The Dropbox file information
   */
  public function upload($file){
    if (is_object($this->client) && $this->client instanceof  Dropbox\Client){
      $f = fopen($file, "rb");
      $result = $this->client->uploadFile("/$file", Dropbox\WriteMode::add(), $f);
      fclose($f);
      return $result;
    }
    return false;
  }
}
