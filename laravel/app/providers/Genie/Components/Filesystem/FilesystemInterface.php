<?php

namespace Genie\Components\Filesystem;


interface FilesystemInterface {

  public function upload($file);
} 