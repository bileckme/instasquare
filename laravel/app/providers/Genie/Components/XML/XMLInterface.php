<?php
namespace Genie\Components\XML;

interface XMLInterface {

  public function getFeed($feed);
  public function getElement($element);
  public function getAttribute($attribute);
  public function getContent($content);
  public function getChildren($children);
  public function toXML($object);
  public function toObject($xml);

}