<?php
namespace Genie;

use Illuminate\Support\ServiceProvider;

/**
 * Class GenieServiceProvider
 */
class GenieServiceProvider extends ServiceProvider {

  protected $app;

  public function register(){
    $this->bindComponents();
  }

  protected function bindComponents(){
    $this->app->bindDataComponent();
    $this->app->bindFilesystemComponent();
    $this->app->bindMigrationComponent();
    $this->app->bindSocialComponent();
    $this->app->bindXMLComponent();
  }

  private function bindDataComponent(){
    $this->app->bind('Genie\Components\Data\DataInterface', 'Genie\Components\Data\DataComponent');
  }

  private function bindFilesystemComponent(){
    $this->app->bind('Genie\Components\Filesystem\FileSystemInterface', 'Genie\Components\Social\FilesystemComponent');
  }

  private function bindMigrationComponent(){
    $this->app->bind('Genie\Components\Migration\MigrationInterface', 'Genie\Components\Migration\MigrationComponent');
  }

	private function bindSocialComponent(){
		$this->app->bind('Genie\Components\Social\SocialInterface', 'Genie\Components\Social\SocialComponent');
	}

  private function bindXMLComponent(){
    $this->app->bind('Genie\Components\Filesystem\FileSystemInterface', 'Genie\Components\Social\FilesystemComponent');
  }

}
