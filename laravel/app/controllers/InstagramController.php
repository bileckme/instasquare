<?php

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;

class InstagramController extends \BaseController
{
  protected $token;
  protected $client;

  /**
   * Instagram Authentication Session
   *
   * @return Response
   */
  public function session()
  {
    $geolocation = array();
    $endpoint = array();
    $provider = new Instagram(Config::get('social.instagram'));

    if (!Input::has('code')) {
      $provider->authorize();
    } else {
      try {
        $token = $provider->getAccessToken('authorization_code', array('code' => Input::get('code')));
        try {
          if (Session::has('gelocation')) {
            $geolocation = Session::get('gelocation');
            $param = http_build_query(array('lat'          => $geolocation['lat'],
                                            'lng'          => $geolocation['lng'],
                                            'access_token' => $token->getToken()));
          } else {
            $param = http_build_query(array('lat'          => '0.0',
                                            'lng'          => '0.0',
                                            'access_token' => $token->getToken()));
          }

          $endpoint['users'] = Config::get('social.instagram.apiUri') .
            Config::get('social.instagram.resource.users');
          $endpoint['locations'] = Config::get('social.instagram.apiUri') .
            Config::get('social.instagram.resource.locations') .
            'search?' . $param;
          Session::put('instagram.oauth.token', $token->getToken());
          Session::put('instagram.users.url', $endpoint['users']);
          Session::put('instagram.locations.url', $endpoint['locations']);

          $signature = $this->signature($endpoint['locations'], explode('&', $param), Config::get('social.instagram.clientSecret'));
          $url = $endpoint['locations'];
          $url = 'https://api.instagram.com/v1/locations/'.Session::get('foursquare.location').'/media/recent';
          $this->client = new Client();
          $response = $this->client->get($url);
          $body = $response->getBody();
          $data = json_decode((string)$body);
          Session::put('photos', $data);
        } catch (RequestException $e) {
          if ($e->getResponse()->getStatusCode() != '200') {
            $json = $e->getResponse()->getBody()->getContents();
            $response = json_decode($json);

            return Redirect::route('login')->withError($response->meta->error_message);
          }
        } catch (Exception $e) {
          Log::warning('Instagram API: unable to get user details');

          return Redirect::route('login')->withError('Instagram API: unable to get user details');
        }
      } catch (Exception $e) {
        Log::warning('Instagram API: unable to get access token');
      }
    }
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    //
  }


  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //
  }


  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    //
  }


  /**
   * Display the specified resource.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
    //
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
    //
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update($id)
  {
    //
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    //
  }

  private function signature($endpoint, $params, $secret)
  {
    $sig = $endpoint;
    ksort($params);
    foreach ($params as $key => $val) {
      $sig .= "|$key=$val";
    }

    return hash_hmac('sha256', $sig, $secret, false);
  }


}
