<?php

class LoginController extends \BaseController
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    if (Input::get('instalogin')) {
      $button = '<a class="btn btn-block" href="/instagram-session">
          				<span class="fa fa-instagram"></span> Instagram
        				</a>';
    } else {

      $button = '<a class="btn btn-block" href="/foursquare-session">
          				<span class="fa fa-foursquare"></span> Foursquare
        				</a>';
    }

    $data = array('button' => $button);

    return View::make('default.login', $data);
  }


  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //
  }


  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    //
  }


  /**
   * Display the specified resource.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
    //
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
    //
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update($id)
  {
    //
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    //
  }


}
