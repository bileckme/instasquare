<?php

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;

class FourSquareController extends \BaseController
{

  protected $token;
  protected $client;

  /**
   * FourSquare Authentication Session
   *
   * @return Response
   */
  public function session()
  {
    $geolocation = array();
    $endpoint = array();
    $provider = new FourSquare(Config::get('social.foursquare'));

    if (!Input::has('code')) {
      $provider->authorize();
    } else {
      try {
        $token = $provider->getAccessToken('authorization_code', array('code' => Input::get('code')));

        try {

          $param = http_build_query(array('v'           => date('Ymd'),
                                          'oauth_token' => $token->getToken()));

          $endpoint['users'] = Config::get('social.foursquare.apiUri') .
            Config::get('social.foursquare.resource.users') . '?' . $param;
          $endpoint['venues'] = Config::get('social.foursquare.apiUri') .
            Config::get('social.foursquare.resource.venues') .
            Config::get('social.foursquare.location') . '?' . $param;
          Session::put('foursquare.oauth.token', $token->getToken());
          Session::put('foursquare.users.url', $endpoint['users']);
          Session::put('foursquare.venues.url', $endpoint['venues']);
          Session::put('foursquare.location', Config::get('social.foursquare.location'));

          $this->client = new Client();
          $response = $this->client->get($endpoint['venues']);
          $body = $response->getBody();
          $data = json_decode((string)$body);
          $geolocation['lat'] = $data->response->venue->location->lat;
          $geolocation['lng'] = $data->response->venue->location->lng;
          Session::put('geolocation', $geolocation);
          echo "Stored in session<br />";
          echo "Lat: " . $geolocation['lat'] . '<br />';
          echo "Lng: " . $geolocation['lng'] . '<br />';

          return Redirect::to('/login?instalogin=1');
        } catch (RequestException $e) {
          if ($e->getResponse()->getStatusCode() != '200') {
            $json = $e->getResponse()->getBody()->getContents();
            $response = json_decode($json);

            return Redirect::route('login')->withError($json);
          }
        } catch (Exception $e) {
          Log::warning('FourSquare API: unable to get user details');
        }
      } catch (Exception $e) {
        Log::warning('FourSquare API: unable to get access token');
      }
    }
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function sample()
  {
    //
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    //
  }


  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //
  }


  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    //
  }


  /**
   * Display the specified resource.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
    //
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
    //
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update($id)
  {
    //
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    //
  }


}
