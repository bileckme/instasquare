<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', array('as' => 'homepage', 'uses' => 'LoginController@index'));
Route::get('/login', array('as' => 'login', 'uses' => 'LoginController@index'));
Route::get('/foursquare-session', array('as' => 'foursquare.session', 'uses' => 'FourSquareController@session'));
Route::get('/instagram-session', array('as' => 'instagram.session', 'uses' => 'InstagramController@session'));


