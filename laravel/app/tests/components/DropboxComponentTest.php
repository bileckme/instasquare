<?php

use Genie\Component\Filesystem\DropboxComponent as Dropbox;


class DropboxComponentTest extends TestCase {

  protected $dropbox;

  public function setUp(){
    $this->dropbox = new Dropbox();
  }

  public function testAccount(){
    $token = Config::get('social.dropbox.token');
    $this->dropbox->getClient($token);

    $account = $this->dropbox->account();
  }

  public function testUpload(){
    $token = Config::get('social.dropbox.token');
    $this->dropbox->getClient($token);

    $file = public_path('robot.txt');
    $upload = $this->dropbox->upload($file);
  }

  public function tearDown(){

  }
} 