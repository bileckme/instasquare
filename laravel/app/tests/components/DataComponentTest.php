<?php

use Genie\Components\Data\DataComponent as Schemata;

class DataComponentTest extends TestCase {
  protected $data;
  protected $connection;
  protected $original;
  protected $duplicate;
  protected $originalTable;
  protected $duplicateTable;

 /**
  * calls setUp method
  */
  public function setUp(){
    parent::setUp();
    $this->data = new Schemata();
    $this->connection = DB::connection('mysql');
    $this->original = 'db_main';   // Database to duplicate 
    $this->duplicate = 'db_copy';  // Duplicated Database
  }
  
  /**
   * Test create a database
   *
   * @return void
   */
  public function testCreateDatabase()
  {
    $this->data->createDatabase($this->duplicate);
  }

  /**
   * Test cloning a database structure
   *
   * @return void
   */
  public function testCloneSchema()
  {
    $this->data->cloneSchema($this->original, $this->duplicate);
  }

  /**
   * Test importing data
   *
   * @return void
   */
  public function testImport()
  {
    $this->data->import($this->original, $this->duplicate);
  }

  /**
   * Test run
   *
   * @return void
   */
  public function testRun()
  {
    $this->data->createDatabase($this->duplicate);
    $this->data->duplicateDatabaseStructure($this->original, $this->duplicate);
    $this->data->import($this->original, $this->duplicate, 1000);
  }

  /**
   * calls tearDown method
   */
  public function tearDown(){
    DB::disconnect();
    unset($this->connection);
    unset($this->original);
    unset($this->duplicate);
    parent::tearDown();
    m::close();
  }
}